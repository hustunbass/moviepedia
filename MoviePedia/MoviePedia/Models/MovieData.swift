//
//  MovieData.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 12.04.2021.
//

import Foundation

struct MovieRequest: Codable {
    var page : Int?
    var results : [Results]?
}

//class MovieRequest: NSObject, NSCoding {
//    var page : Int?
//    var results : [Results]?
//
//    required convenience init?(coder aDecoder: NSCoder) {
//        self.init()
//        self.page = aDecoder.decodeInteger(forKey: "page")
//    }
//        func encode(with aCoder: NSCoder) {
//            aCoder.encode(page!, forKey: "id")
//        }
//
//}
