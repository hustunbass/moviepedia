//
//  Result.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 12.04.2021.
//

import Foundation

//struct Results: Codable {
//    var id : Int?
//    var release_date : String?
//    var title : String?
//    var poster_path : String?
//    var backdrop_path : String?
//    var vote_average : Double?
//    var vote_count : Int?
//}

class Results: Codable, NSCoding {
    var id : Int?
    var release_date : String?
    var title : String?
    var poster_path : String?
    var backdrop_path : String?
    var vote_average : Double?
    var vote_count : Int?

    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.id = aDecoder.decodeInteger(forKey: "id")
        self.release_date = aDecoder.decodeObject(forKey: "release_date") as? String
        self.title = aDecoder.decodeObject(forKey: "title") as? String
        self.poster_path = aDecoder.decodeObject(forKey: "poster_path") as? String
        self.backdrop_path = aDecoder.decodeObject(forKey: "backdrop_path") as? String
        self.vote_average = aDecoder.decodeDouble(forKey: "vote_average")
        self.vote_count = aDecoder.decodeInteger(forKey: "vote_count")
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(id!, forKey: "id")
        aCoder.encode(release_date, forKey: "release_date")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(poster_path, forKey: "poster_path")
        aCoder.encode(backdrop_path, forKey: "backdrop_path")
        aCoder.encode(vote_average, forKey: "vote_average")
        aCoder.encode(vote_count, forKey: "vote_count")
    }

}


//class Results: NSObject, Codable, NSCoding {
//    var id : Int?
//    var release_date : String?
//    var title : String?
//    var poster_path : String?
//    var backdrop_path : String?
//    var vote_average : Double?
//    var vote_count : Int?
//
//
//    override init() {}
//
//
//    init(title:String, poster_path: String) {
////        self.id = id
////        self.release_date = release_date
//        self.title = title
//        self.poster_path = poster_path
////        self.backdrop_path = backdrop_path
//    }
//
//    required convenience init?(coder aDecoder: NSCoder) {
////        let id1 = aDecoder.decodeInteger(forKey: "id")
////        let release_date1 = aDecoder.decodeObject(forKey: "release_date") as? String
//        let title1 = aDecoder.decodeObject(forKey: "title") as? String ?? ""
//        let poster_path1 = aDecoder.decodeObject(forKey: "poster_path") as? String ?? ""
////        let backdrop_path1 = aDecoder.decodeObject(forKey: "backdrop_path") as? String
////        let vote_average = aDecoder.decodeDouble(forKey: "vote_average")
////        let vote_count = aDecoder.decodeInteger(forKey: "vote_count")
//
//        self.init(title: title1, poster_path: poster_path1)
//    }
//
//    func encode(with aCoder: NSCoder) {
////        aCoder.encode(id!, forKey: "id")
////        aCoder.encode(release_date, forKey: "release_date")
//        aCoder.encode(title, forKey: "title")
//        aCoder.encode(poster_path, forKey: "poster_path")
////        aCoder.encode(backdrop_path, forKey: "backdrop_path")
////        aCoder.encode(vote_average, forKey: "vote_average")
////        aCoder.encode(vote_count, forKey: "vote_count")
//    }

//}
