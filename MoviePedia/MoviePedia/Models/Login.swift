//
//  Login.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 14.04.2021.
//

import Foundation

struct Login: Codable {
    var success : Bool? = false
    var expires_at : String?
    var request_token : String?
}
