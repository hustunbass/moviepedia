//
//  MovieDetail.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 13.04.2021.
//

import Foundation

class MovieDetail: NSObject, Codable, NSCoding {
    var id: Int?
    var backdrop_path : String?
    var original_title : String?
    var overview : String?
    var vote_average : Double?
    var vote_count : Int?
    var production_companies : [Production]?
    var genres : [Genres]?
    var poster_path : String?

    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        
        self.id = aDecoder.decodeInteger(forKey: "id")
        self.backdrop_path = aDecoder.decodeObject(forKey: "backdrop_path") as? String
        self.original_title = aDecoder.decodeObject(forKey: "original_title") as? String
        self.overview = aDecoder.decodeObject(forKey: "overview") as? String
        self.vote_average = aDecoder.decodeDouble(forKey: "vote_average")
        self.vote_count = aDecoder.decodeInteger(forKey: "vote_count")
        self.poster_path = aDecoder.decodeObject(forKey: "poster_path") as? String
    }

    func encode(with aCoder: NSCoder) {

        aCoder.encode(id!, forKey: "id")
        aCoder.encode(backdrop_path, forKey: "backdrop_path")
        aCoder.encode(original_title, forKey: "original_title")
        aCoder.encode(overview, forKey: "overview")
        aCoder.encode(vote_average!, forKey: "vote_average")
        aCoder.encode(vote_count!, forKey: "vote_count")
        aCoder.encode(poster_path, forKey: "poster_path")
    }
}
