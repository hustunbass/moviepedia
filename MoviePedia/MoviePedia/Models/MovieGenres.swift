//
//  MovieGenres.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 15.04.2021.
//

import Foundation

struct Genres: Codable {
    var id : Int?
    var name : String?
}
