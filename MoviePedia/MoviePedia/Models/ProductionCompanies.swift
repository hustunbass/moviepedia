//
//  ProductionCompanies.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 15.04.2021.
//

import Foundation

struct Production: Codable {
    var id : Int?
    var logo_path : String?
    var name : String?
    var origin_country : String?
}
