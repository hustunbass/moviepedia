//
//  HomeTableViewCell.swift
//  MoviePedia
//
//  Created by Onur Başdaş on 13.04.2021.
//

import UIKit
import SDWebImage

class HomeTableViewCell: UITableViewCell {
    
    
    @IBOutlet var backView: UIView!
    @IBOutlet var posterImage: UIImageView!
    @IBOutlet var filmName: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var scoreLabel: UILabel!
    @IBOutlet var commentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backView.backgroundColor = .black
        posterImage.layer.borderWidth = 3
        posterImage.layer.borderColor = UIColor.white.cgColor
    }

    func loadData(movies: Results) {
        self.filmName.text = movies.title
        self.dateLabel.text = movies.release_date
        let imageUrl = URL(string: Constants.imageUrl + movies.poster_path!)
        self.posterImage.sd_setImage(with: imageUrl)
        self.scoreLabel.text = "\(movies.vote_average!)"
        self.commentLabel.text = "(\(movies.vote_count!) votes)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
