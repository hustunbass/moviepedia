//
//  DetailGenresCollectionViewCell.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 15.04.2021.
//

import UIKit

class DetailGenresCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var genresButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        genresButton.layer.cornerRadius = 15
        genresButton.titleLabel?.numberOfLines = 0
        genresButton.sizeToFit()
    }
}
