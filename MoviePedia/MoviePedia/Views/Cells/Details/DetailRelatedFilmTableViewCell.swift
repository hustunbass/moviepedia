//
//  DetailRelatedFilmTableViewCell.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 14.04.2021.
//

import UIKit
import SDWebImage

class DetailRelatedFilmTableViewCell: UITableViewCell, UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var relatedFilms = [Results]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(UINib(nibName:"DetailRelatedFilmCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "detailRelatedCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func loadData(data: [Results]) {
        self.relatedFilms = data
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return relatedFilms.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "detailRelatedCell", for: indexPath) as! DetailRelatedFilmCollectionViewCell
        if let image = self.relatedFilms[indexPath.row].poster_path {
            cell.relatedFilmImage.sd_setImage(with: URL(string: Constants.imageUrl + image))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 125.0, height: 156.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

