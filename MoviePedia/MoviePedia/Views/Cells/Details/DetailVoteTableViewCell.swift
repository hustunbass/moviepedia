//
//  DetailVoteTableViewCell.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 14.04.2021.
//

import UIKit

class DetailVoteTableViewCell: UITableViewCell {

    @IBOutlet weak var voteAverage: UILabel!
    @IBOutlet weak var voteCount: UILabel!
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func loadData(data: MovieDetail?) {
        self.voteAverage.text = "\(data?.vote_average ?? 0)"
        self.voteCount.text = "(\(data?.vote_count ?? 0) votes)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
