//
//  DetailProductTableViewCell.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 14.04.2021.
//

import UIKit

class DetailProductTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var productsLabel: UILabel!
    
    var products = [Production]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadData() {
        self.productsLabel.text = ""
        for index in products {
            self.productsLabel.text! += index.name! + ","
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
