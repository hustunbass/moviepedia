//
//  DetailPosterTableViewCell.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 14.04.2021.
//

import UIKit

class DetailPosterTableViewCell: UITableViewCell {

    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var filmTitle: UILabel!
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadData(data: MovieDetail?) {
        self.filmTitle.text = data?.original_title
        if let imageUrl = data?.backdrop_path {
            self.posterImage.sd_setImage(with: URL(string: Constants.imageUrl + imageUrl))
        } else {
            self.posterImage.sd_setImage(with: URL(string: Constants.placeholderImage))
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
