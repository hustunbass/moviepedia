//
//  ShowCaseCollectionViewCell.swift
//  MoviePedia
//
//  Created by Ekrem Özkaraca on 14.04.2021.
//

import UIKit

class ShowCaseCollectionViewCell: UICollectionViewCell {

    static let identifier = "ShowCaseCollectionViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "ShowCaseCollectionViewCell", bundle: nil)
    }
    @IBOutlet weak var collectionView: UIView!
    @IBOutlet weak var collectionImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.layer.masksToBounds = false
        collectionView.layer.cornerRadius = collectionView.frame.size.width / 5
        collectionView.clipsToBounds = true
    }
}
