//
//  ShowCaseTableViewCell.swift
//  MoviePedia
//
//  Created by Ekrem Özkaraca on 14.04.2021.
//

import UIKit
import SDWebImage

class ShowCaseTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    static let identifier = "ShowCaseTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "ShowCaseTableViewCell", bundle: nil)
    }
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var service = Service()
    var movies = [Results]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(ShowCaseCollectionViewCell.nib(), forCellWithReuseIdentifier: ShowCaseCollectionViewCell.identifier)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func getData(typeForFetch : String) {
        print(typeForFetch)
        service.fetchShowCaseData(type: typeForFetch) { (result) in
            if result != nil {
                self.movies.append(contentsOf: result!)
                self.collectionView.reloadData()
            }
        }
    }
//    COLLECTIONVIEW
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ShowCaseCollectionViewCell.identifier, for: indexPath) as! ShowCaseCollectionViewCell
        cell.collectionImageView.sd_setImage(with: URL(string: Constants.imageUrl + movies[indexPath.row].poster_path!))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print(movies[indexPath.row].id!)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 250)
    }
}
