//
//  SmallShowCaseCollectionViewCell.swift
//  MoviePedia
//
//  Created by Ekrem Özkaraca on 16.04.2021.
//

import UIKit

class SmallShowCaseCollectionViewCell: UICollectionViewCell {

    static let identifier = "SmallShowCaseCollectionViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "SmallShowCaseCollectionViewCell", bundle: nil)
    }
    
    @IBOutlet weak var smallView: UIView!
    @IBOutlet weak var smallImageView: UIImageView!
    @IBOutlet weak var smallNameLabel: UILabel!
    @IBOutlet weak var smallDateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        smallImageView.layer.masksToBounds = false
        smallImageView.layer.cornerRadius = smallImageView.frame.size.width / 15
        smallImageView.clipsToBounds = true
    }

}
