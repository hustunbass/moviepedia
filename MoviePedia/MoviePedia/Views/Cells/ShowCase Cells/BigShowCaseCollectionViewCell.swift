//
//  BigShowCaseCollectionViewCell.swift
//  MoviePedia
//
//  Created by Ekrem Özkaraca on 14.04.2021.
//

import UIKit

class BigShowCaseCollectionViewCell: UICollectionViewCell {

    static let identifier = "BigShowCaseCollectionViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "BigShowCaseCollectionViewCell", bundle: nil)
    }
    @IBOutlet weak var bigImageView: UIImageView!
    @IBOutlet weak var bigLabel: UILabel!
    @IBOutlet weak var bigView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bigImageView.layer.masksToBounds = false
        bigImageView.layer.cornerRadius = bigImageView.frame.size.width / 10
        bigImageView.clipsToBounds = true
    }

}
