//
//  SmallShowCaseTableViewCell.swift
//  MoviePedia
//
//  Created by Ekrem Özkaraca on 16.04.2021.
//

import UIKit

class SmallShowCaseTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    static let identifier = "SmallShowCaseTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "SmallShowCaseTableViewCell", bundle: nil)
    }
    var service = Service()
    var movies = [Results]()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SmallShowCaseCollectionViewCell.nib(), forCellWithReuseIdentifier: SmallShowCaseCollectionViewCell.identifier)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func getData(typeForFetch : String) {
        print(typeForFetch)
        service.fetchShowCaseData(type: typeForFetch) { (result) in
            if result != nil {
                self.movies.append(contentsOf: result!)
                self.collectionView.reloadData()
            }
        }
    }

    //    COLLECTIONVIEW
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let smallCell = collectionView.dequeueReusableCell(withReuseIdentifier: SmallShowCaseCollectionViewCell.identifier, for: indexPath) as! SmallShowCaseCollectionViewCell
        smallCell.smallNameLabel.text = movies[indexPath.row].title
        smallCell.smallDateLabel.text = movies[indexPath.row].release_date
        smallCell.smallImageView.sd_setImage(with: URL(string: Constants.imageUrl + movies[indexPath.row].backdrop_path!))
        return smallCell
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print(movies[indexPath.row].id!)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 300, height: 220)
    }
    
}
