//
//  BigShowCaseTableViewCell.swift
//  MoviePedia
//
//  Created by Ekrem Özkaraca on 16.04.2021.
//

import UIKit
import SDWebImage

class BigShowCaseTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    static let identifier = "BigShowCaseTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "BigShowCaseTableViewCell", bundle: nil)
    }
    
    var service = Service()
    var movies = [Results]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(BigShowCaseCollectionViewCell.nib(), forCellWithReuseIdentifier: BigShowCaseCollectionViewCell.identifier)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func getData(typeForFetch : String) {
        print(typeForFetch)
        service.fetchShowCaseData(type: typeForFetch) { (result) in
            if result != nil {
                self.movies.append(contentsOf: result!)
                self.collectionView.reloadData()
            }
        }
    }

    //    COLLECTIONVIEW
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return movies.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let bigCell = collectionView.dequeueReusableCell(withReuseIdentifier: BigShowCaseCollectionViewCell.identifier, for: indexPath) as! BigShowCaseCollectionViewCell
            bigCell.bigLabel.text = movies[indexPath.row].title
            bigCell.bigImageView.sd_setImage(with: URL(string: Constants.imageUrl + movies[indexPath.row].poster_path!))
            return bigCell
        }
        func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
            print(movies[indexPath.row].id!)
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 240, height: 390)
        }
    
}
