//
//  FavoriteTableViewCell.swift
//  MoviePedia
//
//  Created by Ekrem Özkaraca on 21.04.2021.
//

import UIKit
import SDWebImage

class FavoriteTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, PhotoCellDelegate {

    static let identifier = "FavoriteTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "FavoriteTableViewCell", bundle: nil)
    }
    
    @IBOutlet weak var favoriteCollectionView: UICollectionView!
    
    let service = Service()
    var favoriteList = [MovieDetail]()
    var film: MovieDetail?
    override func awakeFromNib() {
        super.awakeFromNib()
        favoriteCollectionView.dataSource = self
        favoriteCollectionView.delegate = self
        favoriteCollectionView.register(FavoriteCollectionViewCell.nib(), forCellWithReuseIdentifier: FavoriteCollectionViewCell.identifier)
        favoriteList = readFavoriteMovies()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func readFavoriteMovies() -> [MovieDetail] {
        let defaults = UserDefaults.standard
        if let data = defaults.object(forKey: "favoriteFilms") as? NSData {
            let movieList = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! [MovieDetail]
            return movieList
        }
        return [MovieDetail]()
    }

    func deleteMovie(movieId: Int) {
        if checkIfFavorites(movieId: movieId) {
            var currentList = readFavoriteMovies()
            currentList.removeAll { (item) -> Bool in
                item.id == movieId
            }
            let defaults = UserDefaults.standard
            let data = NSKeyedArchiver.archivedData(withRootObject: currentList)
            defaults.set(data, forKey: "favoriteFilms")
            defaults.synchronize()
        }
    }
    
    func checkIfFavorites(movieId: Int) -> Bool {
        let movieList = readFavoriteMovies()
        return movieList.filter { (item) -> Bool in
            item.id == movieId
        }.count > 0
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        if let indexPaths = favoriteCollectionView?.indexPathsForVisibleItems {
            for indexPath in indexPaths {
                if let cell = favoriteCollectionView.cellForItem(at: indexPath) as? FavoriteCollectionViewCell {
                    cell.isEditing = editing
                    cell.delegate = self
                }
            }
        }
    }
    func delete(cell: FavoriteCollectionViewCell) {
        if let indexPath = favoriteCollectionView?.indexPath(for: cell) {
            
            let guid = self.favoriteList[indexPath.row].id
            if self.checkIfFavorites(movieId: guid!) {
                self.deleteMovie(movieId: guid!)
            }
            self.favoriteList.remove(at: indexPath.row)
            self.favoriteCollectionView.deleteItems(at: [indexPath])
        }
    }
    
//    COLLECTION VIEW
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favoriteList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let favCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteCollectionViewCell", for: indexPath) as! FavoriteCollectionViewCell
        favCell.favoriteImage.sd_setImage(with: URL(string: Constants.imageUrl + favoriteList[indexPath.row].poster_path!))
        return favCell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 170)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.performSegue(withIdentifier: "toFilmDetail", sender: nil)
        print(favoriteList[indexPath.row].original_title)
    }
    func denemefon() {
        print("dene")
    }
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "toFilmDetail" {
//            if let indexPath = favoriteCollectionView.indexPathsForSelectedItems?.first?.row{
//                let selectedRow = favoriteList[indexPath].id
//                let detailVC = segue.destination as! DetailViewController
//                detailVC.currentPage = selectedRow
//            }
//        }
//    }
}
