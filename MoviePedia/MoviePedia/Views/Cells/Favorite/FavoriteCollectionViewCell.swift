//
//  FavoriteCollectionViewCell.swift
//  MoviePedia
//
//  Created by Onur Başdaş on 13.04.2021.
//

import UIKit
import SDWebImage

protocol PhotoCellDelegate: class {
    func delete(cell: FavoriteCollectionViewCell)
}

class FavoriteCollectionViewCell: UICollectionViewCell {
    @IBOutlet var backView: UIView!
    @IBOutlet var favoriteImage: UIImageView!
    @IBOutlet var closeButton: UIButton!
    
    weak var delegate: PhotoCellDelegate?
    
    static let identifier = "FavoriteCollectionViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "FavoriteCollectionViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backView.layer.cornerRadius = 5
        closeButton.isHidden = !isEditing
//        closeButton.isHidden = true
    }
    
    var isEditing: Bool = false {
        didSet {
            closeButton.isHidden = !isEditing
        }
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        delegate?.delete(cell: self)
    }
}
