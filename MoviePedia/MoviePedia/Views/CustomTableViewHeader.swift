//
//  CustomTableViewHeader.swift
//  MoviePedia
//
//  Created by Onur Başdaş on 13.04.2021.
//

import UIKit

class CustomTableViewHeader: UITableView {
    
    var height : NSLayoutConstraint?
    var bottom : NSLayoutConstraint?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let header = tableHeaderView else {return}
        if let headerLabel = header.subviews.first as? UILabel {
            height = headerLabel.constraints.filter{$0.identifier == "height"}.first
            bottom = header.constraints.filter{$0.identifier == "bottom"}.first
        }
        
        let offSetY = -contentOffset.y
        bottom?.constant = offSetY >= 0 ? 0 :offSetY / 2
        height?.constant = max(header.bounds.height,header.bounds.height + offSetY)
        
        header.clipsToBounds = offSetY <= 0
    }
    
}
