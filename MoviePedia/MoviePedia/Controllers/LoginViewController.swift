//
//  LoginViewController.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 14.04.2021.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var wrongUsernameLabel: UILabel!
    @IBOutlet weak var wrongPasswordLabel: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    
    let service = Service()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        if usernameText.text != "" && passwordText.text != "" {
            self.service.authGetToken { (token) in
                if (token != nil) {
                    self.service.authLogin(userName: self.usernameText.text!, password: self.passwordText.text!, token: (token?.request_token)!) { (authLogin) in
                        if (authLogin != nil) {
                            if authLogin?.success == true {
                                let token = authLogin?.request_token
                                UserDefaults.standard.setValue(token, forKey: "token")
                                self.loadHomeScreen()
                            } else {
                                self.showAlert()
                            }
                        }
                    }
                }
            }
        } else {
            self.wrongUsernameLabel.isHidden = false
            self.wrongPasswordLabel.isHidden = false
        }
    }
    
    func configureUI() {
        usernameText.attributedPlaceholder = NSAttributedString(string: "  Username",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        usernameText.backgroundColor = UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1)
        usernameText.layer.borderWidth = 2
        usernameText.layer.borderColor = UIColor.gray.cgColor
        
        passwordText.attributedPlaceholder = NSAttributedString(string: "  Password",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        passwordText.backgroundColor = UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1)
        passwordText.layer.borderColor = UIColor.gray.cgColor
        loginBtn.layer.cornerRadius = 5
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Warning", message: "Please check your password or username.", preferredStyle: UIAlertController.Style.alert)
        let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func loadHomeScreen(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyBoard.instantiateViewController(identifier: "mainTabBarVC_ID")
        mainTabBarController.modalPresentationStyle = .fullScreen
        self.present(mainTabBarController, animated: true, completion: nil)
        
    }
}
