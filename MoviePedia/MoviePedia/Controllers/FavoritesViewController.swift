//
//  FavoritesViewController.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 12.04.2021.
//

import UIKit
import Alamofire
import SDWebImage

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var favoriteTableView: UITableView!
    @IBOutlet var collectionView: UICollectionView!
    
    let service = Service()
    var film: MovieDetail?
    var favoriteList = [MovieDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favoriteTableView.delegate = self
        favoriteTableView.dataSource = self
        navigationItem.rightBarButtonItem = editButtonItem
        favoriteTableView.register(FavoriteTableViewCell.nib(), forCellReuseIdentifier: FavoriteTableViewCell.identifier)
        favoriteTableView.register(ProfileTableViewCell.nib(), forCellReuseIdentifier: ProfileTableViewCell.identifier)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }

//TABLEVIEW
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let profileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            return profileCell
        case 1:
            let favCell = tableView.dequeueReusableCell(withIdentifier: "FavoriteTableViewCell", for: indexPath) as! FavoriteTableViewCell
            return favCell
        default: break
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 100
        }
        return view.frame.height
    }
//    func readFavoriteMovies() -> [MovieDetail] {
//        let defaults = UserDefaults.standard
//        if let data = defaults.object(forKey: "favoriteFilms") as? NSData {
//            let movieList = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as! [MovieDetail]
//            return movieList
//        }
//        return [MovieDetail]()
//    }
//
//    func deleteMovie(movieId: Int) {
//        if checkIfFavorites(movieId: movieId) {
//            var currentList = readFavoriteMovies()
//            currentList.removeAll { (item) -> Bool in
//                item.id == movieId
//            }
//            let defaults = UserDefaults.standard
//            let data = NSKeyedArchiver.archivedData(withRootObject: currentList)
//            defaults.set(data, forKey: "favoriteFilms")
//            defaults.synchronize()
//        }
//    }
//
//    func checkIfFavorites(movieId: Int) -> Bool {
//        let movieList = readFavoriteMovies()
//        return movieList.filter { (item) -> Bool in
//            item.id == movieId
//        }.count > 0
//    }
//}

//extension FavoritesViewController: UICollectionViewDelegate, UICollectionViewDataSource, PhotoCellDelegate {
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return favoriteList.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let favCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteCollectionViewCell", for: indexPath) as! FavoriteCollectionViewCell
//        favCell.favoriteImage.sd_setImage(with: URL(string: Constants.imageUrl + favoriteList[indexPath.row].poster_path!))
//        return favCell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.performSegue(withIdentifier: "toFilmDetail", sender: nil)
//    }
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "toFilmDetail" {
//            if let indexPath = collectionView.indexPathsForSelectedItems?.first?.row{
//                let selectedRow = favoriteList[indexPath].id
//                let detailVC = segue.destination as! DetailViewController
//                detailVC.currentPage = selectedRow
//            }
//        }
//    }
//
//    override func setEditing(_ editing: Bool, animated: Bool) {
//        super.setEditing(editing, animated: animated)
//
//        if let indexPaths = collectionView?.indexPathsForVisibleItems {
//            for indexPath in indexPaths {
//                if let cell = collectionView.cellForItem(at: indexPath) as? FavoriteCollectionViewCell {
//                    cell.isEditing = editing
//                    cell.delegate = self
//                }
//            }
//        }
//    }
//    func delete(cell: FavoriteCollectionViewCell) {
//        if let indexPath = collectionView?.indexPath(for: cell) {
//
//            let guid = self.favoriteList[indexPath.row].id
//            if self.checkIfFavorites(movieId: guid!) {
//                self.deleteMovie(movieId: guid!)
//            }
//            self.favoriteList.remove(at: indexPath.row)
//            self.collectionView.deleteItems(at: [indexPath])
//        }
//    }
}
