//
//  DetailViewController.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 13.04.2021.
//

import UIKit
import Alamofire
import SDWebImage


class DetailViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var favoriteSelected: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    let service = Service()
    var movies = MovieDetail()
    var relatedFilms = [Results]()
    var film: MovieDetail?
    var currentPage: Int?
    var favoriteList = [MovieDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getMovieDetails()
        favoriteSelected.tag = 0
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    func getMovieDetails() {
        self.configureTableView()
        service.fetchFilmDetail(filmID: currentPage!) { (movie) in
            if let details = movie {
                self.movies = details
                self.getRelatedMovies()
                self.film = details
                if self.checkIfFavorites(movieId: (movie?.id)!) {
                    self.favoriteSelected.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    self.favoriteSelected.tag = 1
                } else {
                    self.favoriteSelected.setImage(UIImage(systemName: "star"), for: .normal)
                    self.favoriteSelected.tag = 0
                }
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func getRelatedMovies() {
        service.fetchRelatedFilms { (results) in
            if let relatedFilms = results {
                self.relatedFilms = relatedFilms
            }
        }
    }
    
    func readFavoriteMovies() -> [MovieDetail] {
        let defaults = UserDefaults.standard
        if let data = defaults.object(forKey: "favoriteFilms") as? NSData {
            let movieList = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as! [MovieDetail]
            return movieList
        }
        return [MovieDetail]()
    }
    
    func checkIfFavorites(movieId: Int) -> Bool {
        let movieList = readFavoriteMovies()
        return movieList.filter { (item) -> Bool in
            item.id == movieId
        }.count > 0
    }
    
    func saveMovie(movie: MovieDetail) {
        if !checkIfFavorites(movieId: movie.id!) {
        var currentList = readFavoriteMovies()
        currentList.append(movie)
        let defaults = UserDefaults.standard
        let data = NSKeyedArchiver.archivedData(withRootObject: currentList)
        defaults.set(data, forKey: "favoriteFilms")
        defaults.synchronize()
        }
    }
    
    func deleteMovie(movieId: Int) {
        if checkIfFavorites(movieId: movieId) {
            var currentList = readFavoriteMovies()
            currentList.removeAll { (item) -> Bool in
                item.id == movieId
            }
            let defaults = UserDefaults.standard
            let data = NSKeyedArchiver.archivedData(withRootObject: currentList)
            defaults.set(data, forKey: "favoriteFilms")
            defaults.synchronize()
        }
    }
    
    @IBAction func test(_ sender: Any) {
        if favoriteSelected.tag == 0 {

            self.saveMovie(movie: film!)
            let image = UIImage(systemName: "star.fill")
            favoriteSelected.setImage(image, for: .normal)
            makeAlert(titleInput: "Film", messageInput: "Favorilere eklenmiştir")
            favoriteSelected.tag = 1
            
        } else {
            self.deleteMovie(movieId: (film?.id)!)
            
            let image = UIImage(systemName: "star")
            favoriteSelected.setImage(image, for: .normal)
            makeAlert(titleInput: "Film", messageInput: "Favorilerden çıkarılmıştır")
            favoriteSelected.tag = 0
        }
    }
    
    func makeAlert(titleInput: String, messageInput: String) {
        let alert = UIAlertController(title: titleInput, message: messageInput, preferredStyle: UIAlertController.Style.alert)
        let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func configureTableView() {
        tableView.separatorStyle = .none
        self.tableView.register(UINib(nibName: "DetailRelatedFilmTableViewCell", bundle: nil), forCellReuseIdentifier: "relatedTableCell")
        self.tableView.register(UINib(nibName: "DetailPosterTableViewCell", bundle: nil), forCellReuseIdentifier: "detailPosterCell")
        self.tableView.register(UINib(nibName: "DetailVoteTableViewCell", bundle: nil), forCellReuseIdentifier: "detailVoteCell")
        self.tableView.register(UINib(nibName: "DetailOverviewTableViewCell", bundle: nil), forCellReuseIdentifier: "detailOverviewCell")
        self.tableView.register(UINib(nibName: "DetailGenresTableViewCell", bundle: nil), forCellReuseIdentifier: "detailGenresTableCell")
        self.tableView.register(UINib(nibName: "DetailProductTableViewCell", bundle: nil), forCellReuseIdentifier: "detailProductCell")
        
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
          return 1
      }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailPosterCell", for: indexPath) as! DetailPosterTableViewCell
            cell.loadData(data: movies)

            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailVoteCell", for: indexPath) as! DetailVoteTableViewCell
            cell.loadData(data: movies)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailOverviewCell", for: indexPath) as! DetailOverviewTableViewCell
            cell.loadData(data: movies)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailGenresTableCell", for: indexPath) as! DetailGenresTableViewCell
            cell.loadData(movies: movies)

            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailProductCell", for: indexPath) as! DetailProductTableViewCell
            if let product = movies.production_companies {
                cell.products = product
            }
            cell.loadData()

            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "relatedTableCell", for: indexPath) as! DetailRelatedFilmTableViewCell
                cell.loadData(data: self.relatedFilms)
            return cell
        default:
            break
        }

        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            return 170.0
        case 1:
            return 60.0
        case 2:
            break
        case 3:
            return 100.0
        case 4:
            break
        case 5:
            return 200.0
        default:
            break
        }
        return 100.0
    }
}
