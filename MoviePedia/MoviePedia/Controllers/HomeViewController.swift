//
//  HomeViewController.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 12.04.2021.
//

import UIKit
import Alamofire
import SDWebImage

class HomeViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var service = Service()
    var movies = [Results]()
    var isMoreDataLoading = false
    var currentPage = 1
    let spinner = UIActivityIndicatorView(style: .large)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        getData()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetails" {
            if let destination = segue.destination as? DetailViewController, let index = tableView.indexPathForSelectedRow {
                destination.currentPage = movies[index.row].id
            }
        }
    }
    
    func configureUI() {
        spinner.color = .white
        let nib = UINib(nibName: "HomeTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "HomeTableViewCell")
        tableView.backgroundColor = .black
    }
    
    func getData() {
        if !self.isMoreDataLoading {
            self.isMoreDataLoading = true
            DispatchQueue.global().async {
                self.service.fetchFilms(page: self.currentPage) { (result) in
                    self.spinner.stopAnimating()
                    if (result != nil) {
                        self.movies.append(contentsOf: result!)
                        self.tableView.reloadData()
                        self.isMoreDataLoading = false
                    }
                }
            }
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        let allMovie = movies[indexPath.row]
        cell.loadData(movies: allMovie)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toDetails", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            spinner.startAnimating()
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            if (!isMoreDataLoading){
                self.currentPage += 1
                self.getData()
            }
        }
    }
}
