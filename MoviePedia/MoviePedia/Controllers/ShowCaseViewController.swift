//
//  ShowCaseViewController.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 12.04.2021.
//

import UIKit

class ShowCaseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    let titles = ["Most Popular Movies", "High Rated Movies", "Best Dramas for 2018", "Latest Movies"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ShowCaseTableViewCell.nib(), forCellReuseIdentifier: ShowCaseTableViewCell.identifier)
        tableView.register(BigShowCaseTableViewCell.nib(), forCellReuseIdentifier: BigShowCaseTableViewCell.identifier)
        tableView.register(SmallShowCaseTableViewCell.nib(), forCellReuseIdentifier: SmallShowCaseTableViewCell.identifier)
    }

//    TABLEVIEW
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1 {
            let bigCell = tableView.dequeueReusableCell(withIdentifier: "BigShowCaseTableViewCell", for: indexPath) as! BigShowCaseTableViewCell
            bigCell.getData(typeForFetch: Constants.type[indexPath.row])
            bigCell.titleLabel.text = titles[indexPath.row]
            return bigCell
        }
         if indexPath.row == 3 {
            let smallCell = tableView.dequeueReusableCell(withIdentifier: "SmallShowCaseTableViewCell", for: indexPath) as! SmallShowCaseTableViewCell
            smallCell.titleLabel.text = titles[indexPath.row]
            smallCell.getData(typeForFetch: Constants.type[indexPath.row])
            return smallCell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowCaseTableViewCell", for: indexPath) as! ShowCaseTableViewCell
        cell.getData(typeForFetch: Constants.type[indexPath.row])
        cell.titleLabel.text = titles[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return 400
        }
        if indexPath.row == 3 {
            return 250
        }
        return 270
    }
}
