//
//  Service.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 12.04.2021.
//

import Foundation
import Alamofire

class Service {
    func fetchFilms(page:Int,callback:@escaping ([Results]?) -> Void){
        AF.request("\(Constants.baseUrl)?api_key=\(Constants.apiKey)&page=\(page)", method: .get,encoding: JSONEncoding.default).response{ response in
            guard let data = response.data else {return}
            do {
                let movieResponse = try JSONDecoder().decode(MovieRequest.self, from:data)
                let movie = movieResponse.results
                callback(movie)
            }catch let e {
                print(e)
            }
        }
    }
    
    func fetchFilmDetail(filmID:Int,callback:@escaping (MovieDetail?) -> Void) {
        AF.request("https://api.themoviedb.org/3/movie/\(filmID)?api_key=2a2163769581037b65b955f4154c0d6b", method: .get,encoding: JSONEncoding.default).response{ response in
            guard let data = response.data else {return}
            do {
                let detailResponse = try JSONDecoder().decode(MovieDetail.self, from:data)
                let detail = detailResponse
                callback(detail)
            }catch let e {
                print(e)
            }
        }
    }
    
    func authGetToken(callback:@escaping(Login?) -> Void) {
        
        AF.request(Constants.loginUrl + "?api_key=\(Constants.apiKey)" , method: .get,encoding: JSONEncoding.default).response{ response in
            guard let data = response.data else {return}
            do {
                let loginResponse = try JSONDecoder().decode(Login.self, from:data)
                let login = loginResponse
                callback(login)
            }catch let e {
                print(e)
            }
        }
    }
    
    func authLogin(userName: String, password: String,token:String, callback: @escaping(Login?) -> Void) {
        
        let params : Parameters = ["username": userName,
                                   "password": password,
                                   "request_token": token]
        let header : HTTPHeaders = ["Content-Type":"application/json",
                                    "Accept":"*/*"]
        
        AF.request(Constants.authlogin + "?api_key=\(Constants.apiKey)" , method: .post,parameters: params,encoding: JSONEncoding.default,headers: header).response{ response in
            guard let data = response.data else {return}
            do {
                let loginResponse = try JSONDecoder().decode(Login.self, from:data)
                let login = loginResponse
                callback(login)
            }catch let e {
                print(e)
            }
        }
    }
    
    func fetchRelatedFilms(callback:@escaping ([Results]?) -> Void){
        AF.request(Constants.relatedMovies, method: .get,encoding: JSONEncoding.default).response{ response in
            guard let data = response.data else {return}
            do {
                let movieResponse = try JSONDecoder().decode(MovieRequest.self, from:data)
                let movie = movieResponse.results
                callback(movie)
            }catch let e {
                print(e)
            }
        }
    }
    
    func fetchShowCaseData(type:String,callback:@escaping ([Results]?) -> Void){
        AF.request("\(Constants.baseUrl)?\(type)&api_key=\(Constants.apiKey)", method: .get,encoding: JSONEncoding.default).response{ response in
            guard let data = response.data else {return}
            do {
                let showCaseResponse = try JSONDecoder().decode(MovieRequest.self, from:data)
                let detail = showCaseResponse.results
                callback(detail)
            }catch let e {
                print(e)
            }
        }
    }
}
