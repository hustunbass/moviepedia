//
//  Constant.swift
//  MoviePedia
//
//  Created by Hakan Üstünbaş on 12.04.2021.
//

import Foundation

class Constants {
    static let baseUrl = "https://api.themoviedb.org/3/discover/movie"
    static let loginUrl = "https://api.themoviedb.org/3/authentication/token/new"
    static let authlogin = "https://api.themoviedb.org/3/authentication/token/validate_with_login"
    static let relatedMovies = "https://api.themoviedb.org/3/discover/movie?with_genres=18&sort_by=vote_average.desc&vote_count.gte=10&api_key=2a2163769581037b65b955f4154c0d6b"
    static let apiKey = "2a2163769581037b65b955f4154c0d6b"
    static let imageUrl = "https://image.tmdb.org/t/p/original"
    static let placeholderImage = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.cdgi.com%2F2019%2F05%2F10-examples-of-fun-404-pages%2F&psig=AOvVaw1rV3vyPyuvCRb3CO26oKnB&ust=1618390042211000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNCL3_Tq-u8CFQAAAAAdAAAAABAV"
    static let type = ["sort_by=popularity.desc",
                       "certification_country=US&certification=R&sort_by=vote_average.desc",
                       "with_genres=18&primary_release_year=2018",
                       "primary_release_date.gte=2020-01-01"]
}

